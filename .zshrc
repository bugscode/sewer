alias pl="mpv"
alias ls="eza -s type"
alias yt="mpv '--ytdl-format=bestvideo[height<=720][vcodec!=vp9]+bestaudio/best' "
alias yta="yt-dlp --extract-audio --audio-format opus --embed-thumbnail"
alias ytd="yt-dlp -f 'bestvideo[height<=720][vcodec!=vp9]+bestaudio/best' --merge-output-format mkv --embed-chapters"

# create && cd into the directory
function mkcd() {
	mkdir -p "$1"
	cd "$1"
}
